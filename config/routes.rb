Rails.application.routes.draw do
  mount Parse::Webhooks, at: '/webhooks'
  root 'home#index'

  resources :accidents
  resources :statuses
  resources :areas
  resources :employees
  resources :companies
  resources :nonconformities
  resources :company_notifications

  get 'finish_action/:id' => 'nonconformities#finish_action', as: 'finish_nonconformity'
  get 'finish_action_accident/:id' => 'accidents#finish_action', as: 'finish_accident'
  get 'users' => 'home#list_users'
  get 'logout' => 'user#end_session'
  get 'convert/:base64' => 'home#convert_base_64'
  get 'sign_in' => 'user#sign_in'
  get 'sign_up' => 'user#sign_up'

  post 'sign_up' => 'user#create_user'
  post 'sign_in' => 'user#create_session'

  delete 'eliminar-usuario/:id' => 'user#delete_user', as: 'delete_user'
end
