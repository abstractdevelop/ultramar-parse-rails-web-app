require 'parse/stack'

# Set your specific Parse keys in your ENV. For all connection options, see
# https://github.com/modernistik/parse-stack#connection-setup
Parse.setup app_id: 'myAppId',
           api_key: 'api',
        master_key: 'ultramar',
        server_url: 'http://34.231.141.239:1337/parse'
        # optional
        #    logging: false,
        #      cache: Moneta.new(:File, dir: 'tmp/cache'),
        #    expires: 1 # cache ttl 1 second


    #QUICK FIX, ISSUE TO SHOW RELATIONS IN VIEWS
    Parse.auto_generate_models!
