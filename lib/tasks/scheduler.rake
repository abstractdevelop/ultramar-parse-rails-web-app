desc "TODO"
  task search_accidents: :environment do
          last_time = Time.now
          if File.exist?('out_accident.txt')
            last_time_old = File.open('out_accident.txt', &:readline)
            accidenthash = Accident.where :created_at.gt => last_time_old.to_time
            accidenthash.each do |col|
                  p "Enviando mail"
                  begin
                    AlertsMailer.accident_created(col.description,col.nameOfInjured,col.employee, col.lat, col.lng).deliver
                    p "Mail enviado"
                    last_time = col.created_at
                    save_document_accident(last_time)
                  rescue exception
                  p "Error al enviar mail #{exception}"
                  end
            end
          else
            Accident.all.each do |col|
                p "Enviando mail"
                 begin
                  AlertsMailer.accident_created(col.description,col.nameOfInjured,col.employee, col.lat, col.lng).deliver
                  p "Mail enviado"
                  last_time = col.created_at
                  save_document_accident(last_time)
                rescue exception
                  p "Error al enviar mail #{exception}"
                end
            end
          end
        end

  desc "TODO"
  task search_nonconformities: :environment do
          last_time = Time.now
          if File.exist?('out_nonconformity.txt')
            last_time_old = File.open('out_nonconformity.txt', &:readline)
            non_conformities_hash = Nonconformity.where :created_at.gt => last_time_old.to_time
            non_conformities_hash.all.each do |col|
                  begin
                    AlertsMailer.nonconformity_created(col.description,col.involved,col.employee, col.lat, col.lng).deliver
                    p "Mail enviado"
                    last_time = col.created_at
                    save_document_nonconformity(last_time)
                  rescue exception
                  p "Error al enviar mail #{exception}"
                  end
            end
          else
            Nonconformity.all.each do |col|
                 begin
                  AlertsMailer.nonconformity_created(col.description,col.involved,col.employee, col.lat, col.lng).deliver
                  p "Mail enviado"
                  last_time = col.created_at
                  save_document_nonconformity(last_time)
                rescue exception
                  p "Error al enviar mail #{exception}"
                end
            end
          end
        end


  private
    def save_document_accident(last_time)
        p "Guardando ultima fecha de revision..."
        File.open("out_accident.txt", "w+") { |file| file.write("#{last_time}") }
        p "(200) - Accidentes revisados con exito"
    end
    def save_document_nonconformity(last_time)
        p "Guardando ultima fecha de revision..."
        File.open("out_nonconformity.txt", "w+") { |file| file.write("#{last_time}") }
        p "(200) - No conformidades revisados con exito"
    end