puts "Upgrading Parse-Server schema collections..."
Parse.auto_upgrade! # Auto_upgrade all models

# Create a sample Parse::User
role = Parse::Role.first_or_create({name: "User"})
role.save


role = Parse::Role.first_or_create({name: "Root"})
role.save

data = {
  username: "administracion@ultramar.cl",
  first_name: "Administracion",
  last_name: "Ultramar",
  email: "administracion@ultramar.cl",
  password: "root112233",
  rolename: "Admin"
}

user = Parse::User.first_or_create(data)
user.signup!

# Status

status = Status.first_or_create({name: "Pendiente",description: "Pendiente"})
status.save

status = Status.first_or_create({name: "En revisión",description: "En revisión"})
status.save

status = Status.first_or_create({name: "Finalizado",description: "Finalizado"})
status.save

# Compañias

teisa = Company.first_or_create({name: "Teisa",email:"teisa@teisa.cl",description:"TEISA tiene como objetivo final brindar el servicio de descarga de los productos desde los camiones, paletizarlos en los elementos apropiados para cada avión, almacenar la carga hasta que sea retirada por la correspondiente línea aérea y, en el caso de productos perecederos, continuar la cadena de frío para mantener la calidad de la exportación. Para ello, cuenta con la infraestructura más adecuada para el resguardo y conservación de todo tipo de carga de exportación por vía aérea, especialmente aquella que se define como perecedera, conjuntamente con un manejo profesional a la altura de las necesidades de los exportadores. Reconociendo siempre la responsabilidad que los productores han puesto en TEISA."})
teisa.save

depocargo = Company.first_or_create({name: "Depocargo",email:"depocargo@depocargo.cl",description:"Depocargo Limitada es una empresa del grupo Ultramar dedicada al almacenamiento de productos de importación que arriban vía aérea al país. A partir de 1988, Ultramar ingresa a la actividad de bodegaje aduanero de importación en el Aeropuerto de Santiago y desde el comienzo se han realizado importantes inversiones en infraestructura, equipos y tecnologías de la información para poder entregar un servicio de excelencia a sus clientes, contribuyendo al mismo tiempo al desarrollo aéreo del país. A partir de 1998, la empresa pasa a llamarse Depocargo."}) 
depocargo.save

asl = Company.first_or_create({name:"asl",email: "asl@asl.cl"})
asl.save

# Areas de asl

area = Area.first_or_create({name:"Despacho Puerta 1",company: asl})
area.save

area = Area.first_or_create({name:"Despacho Puerta 2",company: asl})
area.save

area = Area.first_or_create({name:"Almacenamiento",company: asl})
area.save

area = Area.first_or_create({name:"Internación",company: asl})
area.save

area = Area.first_or_create({name:"Sector Baterías",company: asl})
area.save

area = Area.first_or_create({name:"Losa",company: asl})
area.save

area = Area.first_or_create({name:"Clima USDA",company: asl})
area.save

area = Area.first_or_create({name:"Zona Acuicola",company: asl})
area.save

area = Area.first_or_create({name:"Camara Acuicola",company: asl})
area.save

area = Area.first_or_create({name:"Camara USDA",company: asl})
area.save

area = Area.first_or_create({name:"Descarga",company: asl})
area.save

area = Area.first_or_create({name:"Carga Express",company: asl})
area.save

area = Area.first_or_create({name:"Bodega de Tránsito",company: asl})
area.save

area = Area.first_or_create({name:"Losa",company: asl})
area.save

area = Area.first_or_create({name:"Camara Congelado",company: asl})
area.save

area = Area.first_or_create({name:"Camara Pharma",company: asl})
area.save

area = Area.first_or_create({name:"Sector Baterías",company: asl})
area.save

area = Area.first_or_create({name:"Zona de Enganche",company: asl})
area.save

# Areas de Depocargo

area = Area.first_or_create({name:"Despacho Puerta 1",company: depocargo})
area.save

area = Area.first_or_create({name:"Despacho Puerta 2",company: depocargo})
area.save

area = Area.first_or_create({name:"Almacenamiento",company: depocargo})
area.save

area = Area.first_or_create({name:"Internación",company: depocargo})
area.save

area = Area.first_or_create({name:"Oficina 1° Piso",company: depocargo})
area.save

area = Area.first_or_create({name:"Oficina 2° Piso",company: depocargo})
area.save

area = Area.first_or_create({name:"Camarines",company: depocargo})
area.save

area = Area.first_or_create({name:"Comedor",company: depocargo})
area.save

area = Area.first_or_create({name:"Sector Baterías",company: depocargo})
area.save

area = Area.first_or_create({name:"Losa",company: depocargo})
area.save

# Areas de Teisa
area = Area.first_or_create({name:"Clima USDA",company: teisa})
area.save

area = Area.first_or_create({name:"Zona Acuicola",company: teisa})
area.save

area = Area.first_or_create({name:"Camara Acuicola",company: teisa})
area.save

area = Area.first_or_create({name:"Camara USDA",company: teisa})
area.save

area = Area.first_or_create({name:"Descarga",company: teisa})
area.save

area = Area.first_or_create({name:"Carga Express",company: teisa})
area.save

area = Area.first_or_create({name:"Bodega de Tránsito",company: teisa})
area.save

area = Area.first_or_create({name:"Losa",company: teisa})
area.save

area = Area.first_or_create({name:"Oficina 1° Piso",company: teisa})
area.save

area = Area.first_or_create({name:"Oficina 2° Piso",company: teisa})
area.save

area = Area.first_or_create({name:"Camarines",company: teisa})
area.save

area = Area.first_or_create({name:"Comedor",company: teisa})
area.save

area = Area.first_or_create({name:"Camara Congelado",company: teisa})
area.save

area = Area.first_or_create({name:"Camara Pharma",company: teisa})
area.save

area = Area.first_or_create({name:"Sector Baterías",company: teisa})
area.save

area = Area.first_or_create({name:"Zona de Enganche",company: teisa})
area.save

# Usuarios teisa

userdepocargo = Employee.first_or_create({firstName:"ACEVEDO GARCES",lastName: "ALEX PATRICK",rut: "168094981",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ALDANA ESPINOZA",lastName: "CARLOS ALBERTO",rut: "223332145",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ALMUNA  CASTILLO",lastName: "OSVALDO HERNAN",rut: "127919747",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ALVAREZ  CAVIERES",lastName: "FRANCISCO JAVIER",rut: "105177860",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ANDRADE ESPINOZA",lastName: "DAVID ALONSO",rut: "128544623",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ARAGON CERDA",lastName: "VALERIA ALEJANDRA",rut: "165209044",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ARELLANO GOMEZ",lastName: "JORGE IVAN",rut: "151482562",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"AYALA LILLO",lastName: "LUIS ALEJANDRO",rut: "156698121",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"BAEZ  GONZALEZ",lastName: "MARCIAL ESTEBAN",rut: "143640442",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"BAQUEDANO LOAIZA",lastName: "JULIO ALEJANDRO",rut: "151174604",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"BARRIENTOS TORRES",lastName: "JOSE MIGUEL",rut: "155526998",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"BENITEZ  CARRILLO",lastName: "RAMON CARLOS",rut: "131466447",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"BRUGUERAS HERNANDEZ",lastName: "JHON ERNESTO",rut: "168782640",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CABEZAS REYES",lastName: "CAROLINA ALEJANDRA",rut: "196020500",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CAMPOS CASTILLO",lastName: "ELIAS GUILLERMO",rut: "155694858",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CANCINO ABURTO",lastName: "OSCAR ARMANDO",rut: "144598822",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CARILLANCA PARRA",lastName: "JOSE BERNARDO",rut: "168683898",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CARRASCO VOLLRATH",lastName: "ANA MARIA",rut: "194282141",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CASAS FREDES",lastName: "RODRIGO ALEJANDRO",rut: "167193722",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CASTIGLIONE  ALENCASTRE",lastName: "ALDO",rut: "98964991",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CASTILLO OGAZ",lastName: "JOSE LUIS",rut: "194076703",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CASTRO  CASTRO",lastName: "PATRICIO HUGO",rut: "76613036",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CHEPULICH  OYARZO",lastName: "ARIEL ARTURO",rut: "150684528",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CISTERNA  ALCAMAN",lastName: "MARIA NOELIA",rut: "144920902",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CLEMENTE  FICA",lastName: "MARCO ANTONIO",rut: "95421407",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CONTRERAS  SALINAS",lastName: "CLAUDIA ANDREA",rut: "130155782",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CONTRERAS RAMIREZ",lastName: "STEPHANO PHILIP",rut: "164193942",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CORRALES VALDES",lastName: "MICHAEL FRANCISCO",rut: "14900419K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"CUELLAR",lastName: "MARCELO EDIN",rut: "242430565",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"DIAZ NAVARRO",lastName: "JACOB ELIAS",rut: "137007592",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FARIAS  OYARZUN",lastName: "MAURICIO RAFAEL",rut: "113935804",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FERNANDEZ AZAGRA",lastName: "HUGO ORLANDO",rut: "200867920",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FERNANDEZ CONTRERAS",lastName: "ORLANDO SEBASTIAN",rut: "166260957",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FERREIRA YAÑEZ",lastName: "MAURICIO DE JESUS",rut: "11144238K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FIGUEROA HUILCALEO",lastName: "ALAN EDUARDO",rut: "185975606",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FREDES  AVILES",lastName: "JOSE RENATO",rut: "10338737K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FUENTES CARVAJAL",lastName: "MAXIMO ANDRES",rut: "175096035",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"FUENTES ROJAS",lastName: "CRISTIAN ALEXIS",rut: "170010752",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"GALAZ  FRATTINO",lastName: "ALVARO OMAR",rut: "84267031",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"GATICA ECHAVARRIA",lastName: "JUAN CARLOS",rut: "159985083",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"GOMEZ  YAÑEZ",lastName: "ALEJANDRO ISAAC",rut: "15541801K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"GOMEZ ARENAS",lastName: "KATHERINE PAMELA",rut: "159418545",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"GOROSTIAGA  CÁRDENAS",lastName: "FREDDY CARLOS",rut: "180003207",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"GUZMAN CAAMAÑO",lastName: "GERARDO ANDRES",rut: "133671250",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"HERNANDEZ GONZALEZ",lastName: "SEBASTIAN ALFREDO",rut: "163763583",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"IBARRA VILCHES",lastName: "ESTEBAN ANDRES",rut: "160747129",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ITURRA  VILCHES",lastName: "CARLOS ORLANDO",rut: "97057699",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"LAGOS  CASTILLO",lastName: "MARIO ANTONIO",rut: "125137008",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"LAZCANO  JARA",lastName: "JORGE EDUARDO",rut: "142540959",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"LEAL  TRONCOSO",lastName: "LUIS ROBERTO",rut: "119141427",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"LILLO  VALDIVIA",lastName: "ENZO IVAN",rut: "105514549",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"LILLO VERA",lastName: "DAVID ALEJANDRO",rut: "160301449",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"LIOI  CAMPO",lastName: "INGRID ELIZABETH",rut: "92634329",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MALTES CEBALLOS",lastName: "LUIS ALBERTO",rut: "181079320",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MARICAN CAYULEO",lastName: "ROBINSON GERMAN",rut: "185812855",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MARISCAL  AGUILERA",lastName: "JOSE FRANKLIN",rut: "250265956",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MARTINEZ VICENCIO",lastName: "RODRIGO ANTONIO",rut: "179824655",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MAYENBERGER BEZANILLA",lastName: "HERMANN RONALD AL",rut: "8565439K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MELLADO SALINAS",lastName: "JUAN CRISTIAN",rut: "116284243",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MENDEZ  RAMIREZ",lastName: "LUIS HUMBERTO",rut: "116695146",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MENGHINI YAÑEZ",lastName: "ANDY MICHEL",rut: "161242675",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MIÑOPE UCEDA",lastName: "ERICK JOEL",rut: "244164420",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MONTES VARAS",lastName: "JAIME ANDRES",rut: "161003662",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MORAGA VARGAS",lastName: "JUAN MANUEL",rut: "173412401",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MORENO  DIAZ",lastName: "MARCO ARTURO",rut: "90035177",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MUNITA MERINO",lastName: "FELIPE ANDRES BLAS JULIO",rut: "158009552",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MUÑOZ POBLETE",lastName: "CARLOS ALFREDO",rut: "124074576",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"MUÑOZ PONCE",lastName: "GERMAN ANTONIO",rut: "156658243",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"NAVEAS  MOREIRA",lastName: "VICTOR MANUEL",rut: "102682394",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"NICUL  BARRIOS",lastName: "ZUNILDA ALEJANDRA",rut: "141864351",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ORTIZ DURAN",lastName: "SEBASTIAN IGNACIO",rut: "187260388",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ORTIZ ZAMORANO",lastName: "RENE ESTEBAN",rut: "166177987",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PALOMINO PORLES",lastName: "ROBERTO JOSYMAR",rut: "219360967",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PARDO ESPINOZA",lastName: "MARIO ESTEBAN",rut: "158221470",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PAVEZ SAAVEDRA",lastName: "RICHARD NELSON",rut: "9707559K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PEÑA GARCIN",lastName: "OSCAR ENRIQUE",rut: "65546264",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PEREZ  FOUERE",lastName: "FRANCISCO JAVIER",rut: "157027239",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PEREZ ERRAZURIZ",lastName: "LUIS PATRICIO",rut: "144603133",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PEZOA JARA",lastName: "AXEL ELISEO",rut: "178690256",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PINO RODRIGUEZ",lastName: "HUGO ANTONIO",rut: "158773600",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"PLAZA VALENZUELA",lastName: "FELIPE IGNACIO",rut: "181725338",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"POZO  AGUILAR",lastName: "CLAUDIO ANTONIO",rut: "93507088",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"QUINTULEN CURRIHUINCA",lastName: "JORGE IGNACIO",rut: "138115798",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RETAMAL RETAMAL",lastName: "JAIME ANDRES",rut: "160884770",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RIFFO  IBACETA",lastName: "EDGARDO ANTONIO",rut: "56790462",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RIOS ZAMBRANO",lastName: "ESTEBAN SALOMON",rut: "185004015",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RIVAS CONTRERAS",lastName: "MARCELO JEAN FRANCO",rut: "191745965",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RIVEROS  CORNEJO",lastName: "WILFREDO ANTON",rut: "162251724",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RODRIGUEZ GUZMAN",lastName: "MANUEL ANDRES",rut: "136368273",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ROJAS ROMERO",lastName: "LUIS ANDRES",rut: "16459775K",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ROMAN ESCARATE",lastName: "CRISTIAN RICARDO",rut: "165768981",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ROMERO MEDINA",lastName: "ROLANDO ANTONIO",rut: "256459558",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"ROMO HERRERA",lastName: "JESUS ALBERTO",rut: "192759862",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"RUMATZ  CONTRERAS",lastName: "ALEXIS HAMM",rut: "136802984",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"SALGADO  CASTRO",lastName: "CARLOS EDUARDO",rut: "170567781",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"SALINAS ESCOBAR",lastName: "FERNANDO IGNACIO",rut: "178109766",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"SAMUR  TUMA",lastName: "JASMIN ELISABETH",rut: "82506349",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"SAN MARTIN MUÑOZ",lastName: "ROBERTO JOEL",rut: "173907648",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"SANCHO CALDERON",lastName: "HECTOR MOISES",rut: "160737409",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"SILVA  SEPULVEDA",lastName: "MARIO ALEXIS",rut: "132363994",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"TAPIA RODRIGUEZ",lastName: "HUGO ERASMO",rut: "115250582",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"TORRES  SAAVEDRA",lastName: "PATRICIO RODRIGO",rut: "114662755",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"URRA ITURRA",lastName: "JOSE ALEJANDRO",rut: "178993461",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"URRUTIA  LUQUE",lastName: "PAULA DE LAS MERCEDES",rut: "177783803",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VALDEBENITO ALID",lastName: "FARID EMILIO",rut: "173472692",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VALDES GONZALEZ",lastName: "CLAUDIO ANDRES",rut: "130696546",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VALENCIA MOLINA",lastName: "DANIEL ALEJANDRO",rut: "179036339",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VALENZUELA ARAVENA",lastName: "BERNARDITA DEL CARME",rut: "107743979",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VALENZUELA MADRID",lastName: "CAMILO ANIBAL ANDRES",rut: "183269135",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VELIZ  RAMOS",lastName: "FRANCISCO",rut: "203420056",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VELIZ AQUINO",lastName: "MAXIMILIANO PAOLO",rut: "226209565",company: depocargo}) 
userdepocargo.save
userdepocargo = Employee.first_or_create({firstName:"VILLANUEVA SEPULVEDA",lastName: "NICOLAS ANDRES",rut: "18152782k",company: depocargo}) 
userdepocargo.save

userteisa = Employee.first_or_create({firstName:" ALEX RODRIGO",lastName: "ABARZUA  VALENZUELA",rut: "135859060",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROBENSON",lastName: "ADOU",rut: "252346163",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JONATHAN",lastName: "ADOUX",rut: "255150413",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" BELEN ANDREA",lastName: "AEDO CARRASCO",rut: "157939211",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PABLO ESTEBAN",lastName: "AGUAYO QUIROZ",rut: "186209842",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FREDI ELIAS",lastName: "AGUILAR CARRILLANCA",rut: "75831587",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DAVID ALEJANDRO",lastName: "AGUILAR CISTERNA",rut: "168306199",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PABLO IGNACIO",lastName: "ALBA  LIZANA",rut: "185342085",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DAVID ALEJANDRO",lastName: "ALFARO COFRE",rut: "172481566",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CAMILO HERNÁN",lastName: "ALVARADO  LLANCAMÁN",rut: "156645109",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FELIPE ALFREDO",lastName: "ARANEDA ARAYA",rut: "180973303",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MATEO CRISTOPHER",lastName: "ARANEDA MEDINA",rut: "194850549",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ALIRO",lastName: "ARAVENA  OLAVE",rut: "102158180",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ALFREDO MANUEL",lastName: "ARAVENA  PALMA",rut: "161314013",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FRANCISCO JOSE",lastName: "ARAYA UBILLA",rut: "128041273",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JORGE ANDRES",lastName: "ARAYA  ARROYO",rut: "138956172",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN TOMAS",lastName: "ARAYA  GAJARDO",rut: "115083155",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CESAR MAURICIO",lastName: "ARCE GOMEZ",rut: "167134548",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" WILLIAMS MAX",lastName: "ARENAS COLLAO",rut: "178774816",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SEBASTIAN ALEXIS",lastName: "AVILES TAPIA",rut: "18627573k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUDITH NICOLE",lastName: "AZOCAR  PARRAGUEZ",rut: "188494692",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS ORLANDO",lastName: "BAO URIBE",rut: "15163463k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" KEVIN ROBINSON",lastName: "BARRA TOBAR",rut: "187257107",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS SIGIFREDO",lastName: "BARRIGA  CASTILLO",rut: "144820525",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PEDRO VICTOR",lastName: "BASCONES VERGARA",rut: "237824091",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DIEGO FERNANDO",lastName: "BERRIOS  VILLALOBOS",rut: "92152308",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ANDRES ENRIQUE",lastName: "BOZA SILVA",rut: "176774959",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" OSCAR PATRICO",lastName: "BRAVO  GALDAMES",rut: "96127235",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" IVAN DEL CARMEN",lastName: "CABRERA GODOY",rut: "67326814",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LORETO ALEJANDRA",lastName: "CACERES SOTO",rut: "15472074K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MANUEL EVANDRO",lastName: "CALABRANO  PEREZ",rut: "121267756",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS MARCELO",lastName: "CANDIA  SANCHEZ",rut: "130490182",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" GERMAN LEONEL",lastName: "CANTARERO BERRIOS",rut: "15609384K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANIEL ALFREDO",lastName: "CANTERO REHEL",rut: "165562364",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS HUMBERTO",lastName: "CARO  PONCE",rut: "145432596",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RICARDO",lastName: "CARRILLO ANCAPAN",rut: "102141458",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CRISTIAN MAURICIO",lastName: "CARTES BARAHONA",rut: "114746843",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DIEGO CAMILO",lastName: "CASTILLO CASTILLO",rut: "174738246",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CLAUDIO ANDRES",lastName: "CASTILLO GARCÍA",rut: "172506291",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ANDRES SEBASTIAN",lastName: "CASTRO SAT",rut: "94972434",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CHRISTIAN FERNANDO",lastName: "CASTRO  NANCO",rut: "143288048",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DIONISIO ELEUTERIO",lastName: "CAYUPAN  PAILLAN",rut: "127061874",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" BRAYAN JULIAN",lastName: "CHAMORRO ROLDAN",rut: "247917217",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SANTIAGO ENRIQUE",lastName: "CONCHA  ROJAS",rut: "145738377",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANTE HERNAN",lastName: "CONTRERAS ROJO",rut: "186980360",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN LUIS",lastName: "CONTRERAS  PEÑA",rut: "128911308",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RODRIGO ANDRES",lastName: "CORNEJO PARRA",rut: "132747180",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FELIPE EDUARDO",lastName: "CORREA CARO",rut: "198018902",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MOISES RAUL",lastName: "CORTES GONZALEZ",rut: "170024206",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RUBEN ORLANDO",lastName: "CORTES HERNANDEZ",rut: "170741919",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SEBASTIAN ANTONY",lastName: "CORTES STUMPF",rut: "195652538",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MAURICIO ESTEBAN",lastName: "CUBILLOS ORELLANA",rut: "119476208",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PEDRO DAVID",lastName: "DE LA IGLESIA PADILLA",rut: "160730978",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" GERARD",lastName: "DERISMA",rut: "25224820K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ODIUS",lastName: "DERISMAT",rut: "254384461",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS ENRIQUE",lastName: "DIAZ ITURRIAGA",rut: "121206811",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JHON ARTHUR",lastName: "DIAZ PEREZ",rut: "232086696",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" NORMA ISBEL",lastName: "DIAZ VALENZUELA",rut: "104303498",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SEBASTIAN JESUS",lastName: "DIAZ ZAROR",rut: "181845104",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SEBASTIAN ORLANDO",lastName: "DINAMARCA VERGARA",rut: "163865548",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANILO OCTAVIO",lastName: "DURAN RANIMAN",rut: "132696276",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DIEUMILE",lastName: "EMILE",rut: "242399617",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" VOLONDIER",lastName: "EMILE",rut: "23955251k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN PATRICIO",lastName: "ENCINA BRAVO",rut: "130918220",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SAMUEL ESTEBAN",lastName: "ESCOBAR HERRERA",rut: "156072869",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ANTONIO",lastName: "FARIAS ROMAN",rut: "12645340K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ESTEBAN ANDRES",lastName: "FERNANDEZ PEREZ",rut: "14365706K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" VICTOR ALFONSO",lastName: "FERRADA  VALDES",rut: "162968416",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS ALEJANDRO",lastName: "FLORES CORDOVA",rut: "13504377K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ADINAEL ENRIQUE",lastName: "FUENMAYOR GONZALEZ",rut: "248820225",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MARCO ANTONIO",lastName: "FUENTES GATICA",rut: "109849898",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ARIEL EDUARDO",lastName: "FUENTES ZUÑIGA",rut: "193813496",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MARCELO  ANDRES",lastName: "FUENZALIDA SANCHEZ",rut: "11754284K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" NELSON MANUEL",lastName: "GAETE  RAMIREZ",rut: "98392122",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN ANTONIO",lastName: "GALAZ MILAN",rut: "132618992",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ARTEMIO RODRIGO",lastName: "GALINDO  AGUIRRE",rut: "12249828K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JAIRO DAMIAN",lastName: "GALLEGOS MORALES",rut: "170430735",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PATRICIO RUBEN",lastName: "GALLEGOS  MARTINEZ",rut: "172543979",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ANTONIO",lastName: "GALVEZ  ORQUIERA",rut: "98067930",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" VICTOR HUGO",lastName: "GARAY  GAMBOA",rut: "114800805",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FRANCISCO FLAVIO",lastName: "GARCES  SEGUEL",rut: "127353956",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN DANIEL",lastName: "GOMEZ BARRIENTOS",rut: "186144201",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CLAUDIO ANDRÉS",lastName: "GOMEZ GUZMAN",rut: "126528957",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANIEL ALEJANDRO",lastName: "GONZALEZ DAVILA",rut: "249908797",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ESTEBAN RODRIGO",lastName: "GONZALEZ GONZALEZ",rut: "128666397",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" YERKO WLADIMIR",lastName: "GONZALEZ GUTIERREZ",rut: "163397277",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MATIAS ALEJANDRO",lastName: "GONZALEZ MUÑOZ",rut: "169790671",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MATIAS IGNACIO",lastName: "GONZALEZ RUZ",rut: "197412852",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" EDUARDO ENRIQUE",lastName: "GONZALEZ  DIAZ",rut: "145714605",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOSE ABRAHAM",lastName: "GONZALEZ  TOLEDO",rut: "112953965",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RICHARD GIUSSEPPE",lastName: "GUERRA  MENDOZA",rut: "138611981",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PAUL WILLIAMS",lastName: "GUITARD ALVAREZ",rut: "180791159",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CESAR ANDRES",lastName: "HENRIQUEZ NUÑEZ",rut: "165175468",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RAFAEL MOISES",lastName: "HEREDIA FERNANDEZ",rut: "11946840k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" WALTER JAVIER",lastName: "HERNANDEZ FIGUEROA",rut: "107388583",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LEANDRO ARTURO",lastName: "HERRERA NAVARRO",rut: "181899735",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LIUD BRINCK",lastName: "HERRERA  MADARIAGA",rut: "141642359",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RODRIGO ALEJANDRO",lastName: "HUAQUIMIL HUAIQUINAO",rut: "158275309",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DOMINGO BUENAVENTURA",lastName: "HUENTEN  HUARAPIL",rut: "104814263",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ARMANDO ALBERTO",lastName: "HUIRCAL  TRANGOLAF",rut: "125357555",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MAXDIEL ALBERTO",lastName: "ISLA ISLA",rut: "127034095",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ANDRES ANIBAL",lastName: "JARA CHAVEZ",rut: "135069280",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DIEGO ZACHA",lastName: "JARAMILLO  ZUNIGA",rut: "122493652",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS AGUSTIN",lastName: "LAGOS LARA",rut: "17195665K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" TERESA MARGARITA",lastName: "LAGOS QUIÑEHUAL",rut: "126548958",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" OSCAR ANTONIO",lastName: "LAGOS VALDEBENITO",rut: "16812130K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" WLADIMIR ERNESTO",lastName: "LEIVA  OLEA",rut: "166165008",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JERSON ERIC",lastName: "LLANCAO FLORES",rut: "185431762",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" IGNACIO ALONSO DE JESUS",lastName: "LOPEZ MENESES",rut: "179574012",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS ORLANDO",lastName: "LOPEZ SAAVEDRA",rut: "14334536K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FRANCY",lastName: "LOUIS",rut: "254591173",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ALFREDO",lastName: "MAGUIÑA PORTILLA",rut: "250210000",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MARIO ENRIQUE",lastName: "MALDONADO  GODOY",rut: "109615749",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FABIAN MARCELO",lastName: "MALLEA GONZALEZ",rut: "182006335",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS MAURICIO",lastName: "MAMANI FUENTES",rut: "243670608",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN CARLOS",lastName: "MARTINEZ  OYARCE",rut: "115602861",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ALDO FELIPE",lastName: "MASSA  PEÑA",rut: "108747528",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PEDRO OCTAVIO",lastName: "MAUREIRA  ROSALES",rut: "102151631",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN CLAUDIO",lastName: "MAYORGA  MAYORGA",rut: "104524729",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" NICOLAS ALEJANDRO",lastName: "MENDEZ GARRIDO",rut: "184075199",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN BALTAZAR",lastName: "MIÑOPE  MECHAN",rut: "239301347",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN GABRIEL",lastName: "MOLINA  MALDONADO",rut: "166988365",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DAVID ALEJANDRO",lastName: "MONTOYA  IBARRA",rut: "120769804",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" BENJAMIN IGNACIO",lastName: "MORA CASTRO",rut: "188649831",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN CARLOS",lastName: "MORRIS RAMIREZ",rut: "23669707K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MATIAS",lastName: "MOYANO MARTINEZ",rut: "169325820",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOSE ALEXIS",lastName: "MUÑOZ GALVEZ",rut: "134701463",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" HECTOR ALEJANDRO",lastName: "MUÑOZ JIMENEZ",rut: "138065820",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SERGIO AMBROSIO",lastName: "MUÑOZ  HENRIQUEZ",rut: "118717147",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MANUEL DEL TRANSITO",lastName: "MUÑOZ  LOPEZ",rut: "73104777",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS ALBERTO",lastName: "NAVARRO DELGADO",rut: "153160929",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROGELIO ROBERTO",lastName: "NAVARRO  SÁNCHEZ",rut: "106642052",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOSE DIGNO",lastName: "NIEVE MEDINA",rut: "23692002k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" EMMANUEL",lastName: "NOEL",rut: "253918551",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" GABRIEL RAUL",lastName: "NUÑEZ LEIVA",rut: "163872315",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ALDO GERMAN",lastName: "NUÑEZ PEREZ",rut: "106576955",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" GONZALO ANDRES",lastName: "OLAVARRIA ARRIAGADA",rut: "157824961",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROBERTO ANDRES",lastName: "OLIVARES CANALES",rut: "140111287",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOSE MIGUEL",lastName: "OPAZO CABRERA",rut: "175750274",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CHRISTIAN ALBERTO",lastName: "ORTIZ CAMPILONGO",rut: "246551928",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOSHUA",lastName: "OSORIO OSORIO",rut: "164214559",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CLAUDIO MAURICIO",lastName: "OSORIO  PAZ",rut: "102992814",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RAUL ARTURO",lastName: "OVALLE NAHUELPAN",rut: "141922637",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANTE",lastName: "PAIVA MALCA",rut: "219905939",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN MANUEL",lastName: "PALMA  VIDAL",rut: "9361139K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANILO ARMANDO",lastName: "PARRA VENEGAS",rut: "117547655",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PABLO IVAN",lastName: "PAVEZ CATRILEO",rut: "177703788",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JORGE ALBERTO",lastName: "PEÑA AGUILAR",rut: "94423864",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN ALBERTO",lastName: "PEÑA  MATUS",rut: "107837450",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ERNESTO",lastName: "PEREZ TRONCOSO",rut: "112639241",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FRANKY",lastName: "PETIT DORT",rut: "236799301",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FERNANDO ALEXIS",lastName: "PINO VARGAS",rut: "169225583",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANIEL ALEJANDRO",lastName: "PINTO  CODOCEDO",rut: "150580692",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROBERTO ORLANDO",lastName: "PLAZA SILVA",rut: "138631257",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ORLANDO",lastName: "PONCE GONZALEZ",rut: "157992376",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FRANCISCO FELIPE",lastName: "RAMIREZ RIOS",rut: "17383117K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JUAN PATRICIO",lastName: "RETAMAL  SOTO",rut: "75576978",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROBERTO CARLOS",lastName: "REYES HIDALGO",rut: "117381536",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS ALBERTO",lastName: "REYES  MONTERO",rut: "156183717",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANNY WALTER",lastName: "REYMUNDO ARROYO",rut: "250134576",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PATRICIO ANTONIO",lastName: "RIFFO FAJARDO",rut: "156660175",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS MARCELO",lastName: "RIPETTI  REYES",rut: "85346830",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PAOLA DEL CARMEN",lastName: "RIQUELME  ZUÑIGA",rut: "122484548",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PATRICIA YASMINA",lastName: "ROCHA  ROCHA",rut: "92029352",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS SEBASTIAN",lastName: "RODRIGUEZ  MADARIAGA",rut: "164189147",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JORGE HUMBERTO",lastName: "SAAVEDRA CASTRO",rut: "18838062k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RICARDO ELIAS",lastName: "SALDAÑA VALENZUELA",rut: "178401629",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RAUL ALEJANDRO",lastName: "SALGADO GOMEZ",rut: "114900036",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ELIAS DAVID",lastName: "SANTANDER MARIN",rut: "188578543",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" NICOLAS JAVIER",lastName: "SEGOVIA MENDEZ",rut: "172426166",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" LUIS LIZARDO",lastName: "SEPULVEDA RODRIGUEZ",rut: "164088669",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SEBASTIAN KEVIN",lastName: "SERRA TORRES",rut: "184592711",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" OLIVER ISAAC",lastName: "SERRANO HIDALGO",rut: "165617096",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ORLANDO DANIEL",lastName: "SOBARZO PAILLALEF",rut: "99740760",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" IVAN ALBERTO",lastName: "SOTO VARGAS",rut: "139215893",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FLAVIO ESTEBAN",lastName: "SOTO  LLANOS",rut: "131285760",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" OMAR SEGUNDO",lastName: "SUAREZ PACHECO",rut: "13694505K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANNY JOSE",lastName: "TAPIA BOY",rut: "139219082",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" FRANK DANY",lastName: "TRONCOS ARMIJOS",rut: "236644294",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RONI VICTOR",lastName: "VALDERRAMA VALLADARES",rut: "247012842",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MATIAS",lastName: "VALDES  KUHLMANN",rut: "97845689",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CÉSAR ADOLFO",lastName: "VALENCIA  SALDIVIA",rut: "136976028",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" PATRICIO ALEJANDRO",lastName: "VALVERDE PIZARRO",rut: "172834817",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROBINSON MAURICIO",lastName: "VARGAS ALARCON",rut: "173139950",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" YIMY",lastName: "VASQUEZ HUAYLINOS",rut: "232135948",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" NICOLAS EDUARDO",lastName: "VELIZ ORTEGA",rut: "182968390",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOSE PATRICIO",lastName: "VENEGAS BRAVO",rut: "185562832",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ROXANA IVONNE",lastName: "VENEGAS EPUL",rut: "188465145",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" SERGIO MAURICIO",lastName: "VENEGAS VASQUEZ",rut: "142558718",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" MARCELA ALEJANDRA",lastName: "VENEGAS  EPUL",rut: "15421786K",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" CARLOS ALBERTO",lastName: "VENEGAS  GALARCE",rut: "160886617",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" DANIEL ALEJANDRO",lastName: "VIDELA GONZÁLEZ",rut: "19062675k",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" ABRAHAM JAVIER",lastName: "VILLALOBOS  QUIPALLAN",rut: "172329179",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" RAMON LUIS",lastName: "YAÑEZ HORTA",rut: "184284375",company: teisa}) 
userteisa.save
userteisa = Employee.first_or_create({firstName:" JOHNNY ELMER",lastName: "YZQUIERDO SANCHEZ",rut: "22335999K",company: teisa}) 
userteisa.save

userasl = Employee.first_or_create({firstName:"ACEVEDO  SILVA",lastName: "MAURICIO ANIBAL",rut: "139500512",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"ACEVEDO MIRANDA",lastName: "JOSE LUIS",rut: "119775035",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"ACUNA  SOTO",lastName: "MARIA EUGENIA",rut: "6816429K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"ALMENDRAS RODRIGUEZ",lastName: "VICTOR HUGO",rut: "134386991",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"ARAYA ROMERO",lastName: "NICOLAS MATIAS",rut: "172036317",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"ARTEAGA BRACHO",lastName: "DOUGLAS MIGUEL",rut: "246128723",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"BIAGINI SCIANCA",lastName: "MARIA FERNANDA",rut: "99775564",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"BUSTOS MARTINEZ",lastName: "MARCELO ALEJANDRO",rut: "134535857",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CALDERON MORENO",lastName: "ERNESTO ADOLFO",rut: "63694703",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CANDIA RODRIGUEZ",lastName: "JUAQUIN EDUARDO",rut: "89970342",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CANIULLAN LEPIN",lastName: "ANA MARCELA",rut: "126464495",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CARRASCO  GUZMÁN",lastName: "GERMÁN JOSÉ",rut: "145256518",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CASAS GONZALEZ",lastName: "FELIPE AGUSTIN",rut: "155459514",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CATALAN  CUBILLOS",lastName: "JOSE ORLANDO",rut: "102151895",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CERECEDA  ORMAZABAL",lastName: "PATRICIA VICTO",rut: "69887813",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"CHIU  MOLANPHY",lastName: "TATIANA AISHA",rut: "166561280",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"COBS  PUEYES",lastName: "NILSON",rut: "121713470",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"COFRE  IMIGUALA",lastName: "JUAN ALBERTO",rut: "133532587",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"COLLINAO  GUTIERREZ",lastName: "FIDEL ARTURO",rut: "103133645",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"DIRSCH  ZOLLNER",lastName: "MARIANNE MARGRET",rut: "66915409",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GAMBOA FIERRO",lastName: "MARIO LUIS",rut: "90381296",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GARCIA  BARAONA",lastName: "MARIA VIRGINIA",rut: "62859415",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GEISER  HERNÁNDEZ",lastName: "ROSMARIE INGRID",rut: "5718454K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GODOY  INOSTROZA",lastName: "REINALDO DAVID",rut: "179543915",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GONZALEZ  ABADIE",lastName: "CRISTIAN ALBERTO",rut: "90315994",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GONZALEZ  AZOCAR",lastName: "VALERIA ANDREA",rut: "99117117",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GONZALEZ  PINOCHET",lastName: "CARMEN GLORIA",rut: "153433798",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GONZALEZ UBILLA",lastName: "RODRIGO ANDRES",rut: "134556838",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GUAJARDO CARRASCO",lastName: "ARIEL ALEJANDRO",rut: "154204113",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"GUZMAN HERRERA",lastName: "ERICK GERALD",rut: "167244734",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"HARDESSEN  EBEL",lastName: "VICTOR EDUARDO",rut: "83286989",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"HERNANDEZ MANRIQUEZ",lastName: "PAOLA ANDREA",rut: "170255194",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"HERRERA  LEGUA",lastName: "MARIO GUSTAVO",rut: "123997794",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"HERRERA  ZALDUONDO",lastName: "MARCELINO RAMON",rut: "94959500",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"HUEICHAO AREVALO",lastName: "RODRIGO ANTONIO",rut: "145437539",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"JER  LUCERO",lastName: "AXEL VOLKER",rut: "130527485",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"KLEIN  THIERS",lastName: "VERENA NELLY",rut: "82090185",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"LARENAS RICKE",lastName: "FELIPE ALEJANDRO",rut: "173488726",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MARCHELLI CASTRO",lastName: "ANDREA LISA",rut: "186348842",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MARDONES  MARIHUAL",lastName: "DANIELA",rut: "160929588",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MARDONES JIMENEZ",lastName: "RICARDO ANTONIO",rut: "6467790K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MARIN FERREIRA",lastName: "PASCAL LILIANA",rut: "134416262",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MAUREIRA  GOMEZ",lastName: "GONZALO ESTEBAN",rut: "135486183",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MEINHARD LOPEZ",lastName: "LUZIANNA EUGENIA DE LAS",rut: "253610891",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MICHILLANCA SILVA",lastName: "CARLOS ALEJANDRO",rut: "136942921",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"MORALES OYARCE",lastName: "LUIS GASTON",rut: "13446383K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"NARVAEZ SOTO",lastName: "ELISEO JUAN PABLO",rut: "166576172",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"OLIVERA SOTO",lastName: "PATRICIO MOISES",rut: "139068491",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PACHECO  TORRES",lastName: "ESTEBAN MAURICIO",rut: "157941321",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PASTENE RODRIGUEZ",lastName: "LUIS PATRICIO",rut: "70148862",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PEÑA FUENTES",lastName: "MARIO HERNAN",rut: "173741766",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PEREZ  KAEMPFER",lastName: "NICANOR PABLO",rut: "10314101K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PEREZ  MANCILLA",lastName: "MIROSLAVA DEL ROSARIO",rut: "162431498",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PETERS  GALLARDO",lastName: "MARIA EUGENIA",rut: "6742137K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"PIZARRO MANDIC",lastName: "SEBASTIAN EDUARDO",rut: "192440572",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"POLANCO  ROJAS",lastName: "MIGUEL ALEJANDRO",rut: "157020579",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"QUEZADA  MONTECINO",lastName: "ADOLFO ALFREDO",rut: "106758417",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"QUINTANA  CAVADA",lastName: "JUAN ALBERTO",rut: "106433577",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"RHEINEN  WOLF",lastName: "GUILLERMO ERNESTO",rut: "63776467",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"RIOS  VALDES",lastName: "MARCELO RODRIGO",rut: "128655913",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"RUIZ  GONZALEZ",lastName: "CLAUDIO RAFAEL",rut: "96747322",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"SALINAS QUEZADA",lastName: "INGRID EDITH",rut: "153342059",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"SCHEEL  FRANKE",lastName: "MARIANA",rut: "9486772K",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"SILVA  LAZCANO",lastName: "PATRICIA DEL CARMEN",rut: "71401995",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"SIMI  LUARTE",lastName: "ANGELICA BEATRIZ",rut: "126497768",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"SOZA MORALES",lastName: "ERNESTO JORGE",rut: "126878095",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"TEJOS OLIVOS",lastName: "SERGIO EDGARDO",rut: "145248469",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"TORO GUTIERREZ",lastName: "EMILIO ANTONIO",rut: "100463962",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"TULCAN  ORMAZA",lastName: "GINA ISABEL",rut: "146642543",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"VASQUEZ KUHLMANN",lastName: "JUAN PABLO",rut: "93262417",company: asl}) 
userasl.save
userasl = Employee.first_or_create({firstName:"VILLAVICENCIO  GROISMAN",lastName: "ANDRES",rut: "166611989",company: asl}) 
userasl.save

puts " Se genero sin problemas"