require 'test_helper'

class CompanyNotificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @company_notification = company_notifications(:one)
  end

  test "should get index" do
    get company_notifications_url
    assert_response :success
  end

  test "should get new" do
    get new_company_notification_url
    assert_response :success
  end

  test "should create company_notification" do
    assert_difference('CompanyNotification.count') do
      post company_notifications_url, params: { company_notification: {  } }
    end

    assert_redirected_to company_notification_url(CompanyNotification.last)
  end

  test "should show company_notification" do
    get company_notification_url(@company_notification)
    assert_response :success
  end

  test "should get edit" do
    get edit_company_notification_url(@company_notification)
    assert_response :success
  end

  test "should update company_notification" do
    patch company_notification_url(@company_notification), params: { company_notification: {  } }
    assert_redirected_to company_notification_url(@company_notification)
  end

  test "should destroy company_notification" do
    assert_difference('CompanyNotification.count', -1) do
      delete company_notification_url(@company_notification)
    end

    assert_redirected_to company_notifications_url
  end
end
