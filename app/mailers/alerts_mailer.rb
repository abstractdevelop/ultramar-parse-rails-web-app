class AlertsMailer < ApplicationMailer
  default from: 'notification@abstract.cl'

  def accident_created(picture, description, involved, name, first_name, last_name, lat, lng, company_name)
    @picture = picture
    @lat = lat
    @lng = lng
    @involved = involved
    @description = description
    @name = name
    @first_name = first_name
    @last_name = last_name
    @company_name = company_name
    company = Parse::Query.new('Company')
    mails = company.where name: @company_name
    mails = mails.first.company_notifications
    list = ''
    mails.each do |mail|
      list += "#{mail.user.email},"
    end
    mail(to: list, subject: "Reporte Incidente: Informado por #{first_name} #{last_name}")
  end

  def nonconformity_created(picture, description, name, first_name, last_name, lat, lng, company_name)
    @picture = picture
    @lat = lat
    @lng = lng
    @description = description
    @name = name
    @first_name = first_name
    @last_name = last_name
    @company_name = company_name
    company = Parse::Query.new('Company')
    mails = company.where name: @company_name
    mails = mails.first.company_notifications
    list = ''
    mails.each do |mail|
      list += "#{mail.user.email},"
    end
    mail(to: list, subject: "Reporte No Conformidad: Informado por #{first_name} #{last_name}")
  end
end
