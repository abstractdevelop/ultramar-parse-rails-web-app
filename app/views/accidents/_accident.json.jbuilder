json.extract! accident, :id, :created_at, :updated_at
json.url accident_url(accident, format: :json)
