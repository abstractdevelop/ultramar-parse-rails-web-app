json.extract! company_notification, :id, :created_at, :updated_at
json.url company_notification_url(company_notification, format: :json)
