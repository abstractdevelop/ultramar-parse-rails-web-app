json.extract! nonconformity, :id, :employee_id, :area_id, :description, :lat, :lng, :status_id, :created_at, :updated_at
json.url nonconformity_url(nonconformity, format: :json)
