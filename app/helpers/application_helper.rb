module ApplicationHelper
  def status_to_id(status)
    case status
      when "Pendiente"
        return 0
      when "En revisión"
        return 1
      else
        return 2
    end
  end

  def i_am_admin?
    @user.is_admin
  end

  def i_am_supervisor?
    @user.is_supervisor
  end

end
