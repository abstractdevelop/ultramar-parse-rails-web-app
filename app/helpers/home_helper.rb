module HomeHelper
  def bool_to_s_permissions(user)
    if(user.is_admin)
      "Administrador"
    elsif(user.is_supervisor)
      "Supervisor"
    else
      "Usuario"
    end
  end
end
