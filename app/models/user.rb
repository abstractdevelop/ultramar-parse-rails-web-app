
# The Parse _User collection
class Parse::User < Parse::Object
  # add additional properties
  property :first_name, :string
  property :last_name, :string
  property :is_admin, :bool
  property :is_supervisor, :bool
  property :can_view_all, :bool
end
