
class Nonconformity < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "Nonconformity"

  property :description, :string
  property :lat, :float
  property :lng, :float
  property :involved, :string
  property :picture, :file
  property :priority, :integer
  property :viewed, :string
  property :created_at_viewed, :date
  belongs_to :employee
  belongs_to :area
  belongs_to :status
  belongs_to :company

  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks
  # define a before save webhook for Nonconformity

  ## define an after save webhook for Nonconformity
  #
  webhook :after_save do
    nonconformity = parse_object
    picture = nonconformity.picture.url unless nonconformity.picture.nil?
    company_name = nonconformity.employee.company.fetch['name']
    company_name = nonconformity.company.fetch['name'] unless nonconformity.company.nil?
    AlertsMailer.nonconformity_created(picture, nonconformity.description, nonconformity.involved, nonconformity.employee.first_name, nonconformity.employee.last_name, nonconformity.lat, nonconformity.lng, company_name).deliver_later
    puts 'Mail enviado'
  end

  webhook :before_save do
    nonconformity = parse_object
    # ####test
    pp nonconformity
    unless nonconformity.base64.nil?
      if nonconformity.base64.try(:collection) != ['']
        require 'base64'
        decode_base64_content = Base64.decode64(nonconformity.base64.collection.first)
        filename = "#{Time.now}.png"
        File.open(filename, 'wb') do |f|
          f.write(decode_base64_content)
        end
        file = File.open(filename)
        contents = file.read
        file = Parse::File.new('picture.png', contents, 'image/jpeg')
        file.save
        nonconformity.picture = file
        nonconformity.base64 = ''
        nonconformity
      end
    end
    nonconformity
  end

  def month
    created_at.month
  end

  ## define a before delete webhook for Nonconformity
  # webhook :before_delete do
  #   nonconformity = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for Nonconformity
  # webhook :after_delete do
  #   nonconformity = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end
end
