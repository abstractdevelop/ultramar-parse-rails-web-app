
class Area < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "Area"
  property :name, :string
  property :description, :string
  has_many :accidents
  has_many :nonconformities
  belongs_to :company
  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks
  # define a before save webhook for Area

  def name_with_company
    "#{name} - #{company.name}"
  end
  ## define an after save webhook for Area
  #
  # webhook :after_save do
  #   area = parse_object
  #
  # end

  ## define a before delete webhook for Area
  # webhook :before_delete do
  #   area = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for Area
  # webhook :after_delete do
  #   area = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end

end
