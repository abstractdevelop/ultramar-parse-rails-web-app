
class Status < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "Status"

  property :name, :string
  property :description, :string
  has_many :accidents
  has_many :nonconformities
  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks
  # define a before save webhook for Status
  #webhook :before_save do
  #  status = parse_object
    # perform any validations with status
    # use `error!(msg)` to fail the save
    # ...
  #  status
  #end

  ## define an after save webhook for Status
  #
  # webhook :after_save do
  #   status = parse_object
  #
  # end

  ## define a before delete webhook for Status
  # webhook :before_delete do
  #   status = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for Status
  # webhook :after_delete do
  #   status = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end

end
