
class Accident < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "Accident"

  property :description, :string
  property :name_of_injured, :string
  property :lat, :float
  property :lng, :float
  property :picture, :file
  property :priority, :integer
  property :viewed, :string
  property :involved, :string
  property :created_at_viewed, :date
  belongs_to :area
  belongs_to :company
  belongs_to :employee
  belongs_to :status

  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks
  # define a before save webhook for Accident
  webhook :after_save do
    accident = parse_object
    company_name = accident.employee.company.fetch['name']
    company_name = accident.company.fetch['name'] unless accident.company.nil?
    picture = accident.picture.url unless accident.picture.nil?
    AlertsMailer.accident_created(picture, accident.description, accident.involved, accident.name_of_injured, accident.employee.first_name, accident.employee.last_name, accident.lat, accident.lng, company_name).deliver_later
    puts 'Mail enviado'
  end

  webhook :before_save do
    accident = parse_object
    unless accident.base64.nil?
      if accident.base64.try(:collection) != ['']
        accident = parse_object
        require 'base64'
        decode_base64_content = Base64.decode64(accident.base64.collection.first)
        filename = "#{Time.now}.png"
        File.open(filename, 'wb') do |f|
          f.write(decode_base64_content)
        end
        file = File.open(filename)
        contents = file.read
        file = Parse::File.new('myimage.jpg', contents, 'image/jpeg')
        file.save
        accident.picture = file
        accident.base64 = ''
        pp accident
        accident
      end
    end
    accident
  end

  def month
    created_at.month
  end

  ## define an after save webhook for Accident
  #

  # webhook :after_create do
  #    accident = Accident.last
  #    accident.status = Status.first.id
  #    accident.save
  # end

  ## define a before delete webhook for Accident
  # webhook :before_delete do
  #   accident = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for Accident
  # webhook :after_delete do
  #   accident = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end
end
