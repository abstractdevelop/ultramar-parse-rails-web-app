
class CompanyNotification < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "CompanyNotification"
  
  belongs_to :company
  belongs_to :user

  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks
  # define a before save webhook for CompanyNotification
  webhook :before_save do
    company_notification = parse_object
    # perform any validations with company_notification
    # use `error!(msg)` to fail the save
    # ...
    company_notification
  end

  ## define an after save webhook for CompanyNotification
  #
  # webhook :after_save do
  #   company_notification = parse_object
  #
  # end

  ## define a before delete webhook for CompanyNotification
  # webhook :before_delete do
  #   company_notification = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for CompanyNotification
  # webhook :after_delete do
  #   company_notification = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end

end
