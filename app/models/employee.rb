
class Employee < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "Area"

  property :first_name, :string
  property :last_name, :string
  property :rut, :string
  property :email, :string
  property :phone, :string
  belongs_to :company
  has_many :accidents
  has_many :nonconformities
  property :can_view_all_areas, :boolean


  def full_name
    "#{first_name} #{last_name}"
  end

  def see_company_id
    "#{company.id}"
  end
  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks

  ## define an after save webhook for Area
  #
  # webhook :after_save do
  #   area = parse_object
  #
  # end

  ## define a before delete webhook for Area
  # webhook :before_delete do
  #   area = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for Area
  # webhook :after_delete do
  #   area = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end

end
