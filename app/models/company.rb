
class Company < Parse::Object
  # See: https://github.com/modernistik/parse-stack#defining-properties

  # You can change the inferred Parse table/collection name below
  # parse_class "Company"

  property :name, :string
  property :description, :string
  property :email, :string
  has_many :areas
  has_many :employees
  has_many :users
  has_many :company_notifications
  # See: https://github.com/modernistik/parse-stack#cloud-code-webhooks
  # define a before save webhook for Company
  #webhook :before_save do
  #  company = parse_object
    # perform any validations with company
    # use `error!(msg)` to fail the save
    # ...
  #   company
  #end

  ## define an after save webhook for Company
  #
  # webhook :after_save do
  #   company = parse_object
  #
  # end

  ## define a before delete webhook for Company
  # webhook :before_delete do
  #   company = parse_object
  #   # use `error!(msg)` to fail the delete
  #   true # allow the deletion
  # end

  ## define an after delete webhook for Company
  # webhook :after_delete do
  #   company = parse_object
  # end

  ## Example of a CloudCode Webhook function
  ## define a `helloWorld` Parse CloudCode function
  # webhook :function, :helloWorld do
  #   "Hello!"
  # end

end
