class AccidentsController < ApplicationController
  before_action :set_accident, only: %i[show edit update destroy]
  before_action :set_user
  layout 'admin'
  require 'will_paginate/array'
  # GET /accidents
  # GET /accidents.json
  def index
    @accidents = Parse::Query.new('Accident')

    @accidents.where :employee.id => params[:employee_id] if params[:employee_id].present?
    @accidents.where :status.id => params[:status_id] if params[:status_id].present?
    @accidents.where :area.id => params[:area_id] if params[:area_id].present?

    @accidents.where :company.id => @user.company.id unless @user.is_admin || @user.can_view_all

    unless i_am_admin?
      @accidents = []
      employee_ids = []

      @user.company.employees.each do |employee|
        employee_ids << employee.id
      end

      accidents_company = Parse::Query.new('Accident')
      accidents_company.where :company.id => @user.company.id

      accidents_ids = []
      accidents_company.each { |s| accidents_ids << s.id }

      accidents_employee = Parse::Query.new('Accident')

      accidents_employee.where :id.not => accidents_ids
      accidents_employee.where :employee.id => employee_ids
      accidents_employee.where :company.null => true

      begin
        accidents += accidents_employee.results
      rescue
      # ignored
      ensure
      end

      begin
        accidents += accidents_company.results
      rescue
      # ignored
      ensure
      end

      @accidents += accidents
    end
    if params[:company_id].present?
      @accidents_arr = []
      @accidents.each do |acc|
        if acc.try(:employee).try(:company).try(:id) == params[:company_id]
          @accidents_arr << acc
        end
      end
      @accidents = @accidents_arr
    end
    @accidents = @accidents.sort_by(&:created_at).reverse!.sort_by(&:priority)
    @accidents = @accidents.paginate(page: params[:page], per_page: 10)
  end

  # GET /accidents/1
  # GET /accidents/1.json
  def show
    if @accident.status.fetch['name'] == 'Pendiente'
      @accident.viewed = "#{@user.first_name} #{@user.last_name}"
      @accident.created_at_viewed = Time.now
      @accident.status = Status.first_or_create(name: 'En revisión')
      @accident.priority = 1
      @accident.save!
      @viewed = @accident.viewed
      @created_at_viewed = @accident.created_at_viewed
    else
      @viewed = @accident.viewed
      @created_at_viewed = @accident.created_at_viewed
      if @accident.status.fetch['name'] != 'Finalizado'
        @accident.viewed = "#{@user.first_name} #{@user.last_name}"
        @accident.created_at_viewed = Time.now
        @accident.status = Status.first_or_create(name: 'En revisión')
        @accident.save!
      end
    end
  end

  def finish_action
    @accident = Accident.find(params[:id])
    @accident.status = Status.first_or_create(name: 'Finalizado')
    @accident.priority = 2
    respond_to do |format|
      if @accident.save!
        format.html { redirect_to @accident, notice: 'Nonconformity was successfully created.' }
      else
        format.js { flash[:notice] = 'Error al cambiar el estado' }
      end
    end
  end

  # GET /accidents/new
  def new
    @accident = Accident.new
  end

  # GET /accidents/1/edit
  def edit; end

  # POST /accidents
  # POST /accidents.json
  def create
    @accident = Accident.new(accident_params)
    employee = Employee.find(params[:accident][:employee])
    area = Area.find(params[:accident][:area])
    status = Status.find(params[:accident][:status])
    @accident.employee = employee
    @accident.area = area
    @accident.status = status
    @accident.priority = 0
    respond_to do |format|
      if @accident.save
        format.html { redirect_to @accident, notice: 'Accident was successfully created.' }
        format.json { render :show, status: :created, location: @accident }
      else
        format.html { render :new }
        format.json { render json: @accident.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accidents/1
  # PATCH/PUT /accidents/1.json
  def update
    respond_to do |format|
      employee = Employee.find(params[:accident][:employee])
      status = Status.find(params[:accident][:status])
      area = Area.find(params[:accident][:area])
      @accident.description = params[:accident][:description]
      @accident.name_of_injured = params[:accident][:name_of_injured]
      @accident.lat = params[:accident][:lat]
      @accident.lng = params[:accident][:lng]
      @accident.employee = employee
      @accident.area = area
      @accident.status = status
      @accident.priority = 0
      if @accident.save
        format.html { redirect_to @accident, notice: 'Accident was successfully updated.' }
        format.json { render :show, status: :ok, location: @accident }
      else
        format.html { render :edit }
        format.json { render json: @accident.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accidents/1
  # DELETE /accidents/1.json
  def destroy
    @accident.destroy
    respond_to do |format|
      format.html { redirect_to accidents_url, notice: 'Accident was successfully destroyed.' }
      format.json { head :no_content }
    end
    end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_accident
    @accident = Accident.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def accident_params
    # Parse server dont allow strong parameters
    data = {
      description: params[:accident][:description],
      name_of_injured: params[:accident][:name_of_injured],
      lat: params[:accident][:lat],
      lng: params[:accident][:lng]
    }
  end

  def status_to_id(status)
    case status
    when 'Pendiente'
      0
    when 'En revisión'
      1
    else
      2
      end
  end
end
