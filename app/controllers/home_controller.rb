class HomeController < ApplicationController
  before_action :set_user
  layout 'admin'
  def index
    @non_conformities = Nonconformity.all order: [:created_at.desc]
    @non_conformities = @non_conformities.map(&:month)
    @non_conformities = @non_conformities.uniq.inject([]){|r, i| r << { i => @non_conformities.select{ |b| b == i }.size } }

    @accidents = Accident.all order: [:created_at.desc]
    @accidents = @accidents.map(&:month)
    @accidents = @accidents.uniq.inject([]){|r, i| r << { i => @accidents.select{ |b| b == i }.size } }
  end

  def list_users
    @users = Parse::User.where :company.id => @user.company.id if @user.is_supervisor
    @users = Parse::User if @user.is_admin
    @users = @users.where :email.not => @user.email
  end

  def convert_base_64()
    require "base64"
    decode_base64_content = Base64.decode64("")
    File.open("Output.png", "wb") do |f|
      f.write(decode_base64_content)
    end
  end


end
