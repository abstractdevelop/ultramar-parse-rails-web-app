class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def set_user
    @user = Parse::User.session(session[:session_token])
    @user.email
  rescue => error
    @user.logout unless @user.nil?
    redirect_to sign_in_path
  end

  def i_am_admin?
    @user.is_admin
  end
end
