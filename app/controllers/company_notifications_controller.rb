class CompanyNotificationsController < ApplicationController
  before_action :set_user
  before_action :set_company_notification, only: %i[show edit update destroy]
  layout 'admin'

  # GET /company_notifications
  # GET /company_notifications.json
  def index
    @company_notifications = CompanyNotification.all
  end

  # GET /company_notifications/1
  # GET /company_notifications/1.json
  def show; end

  # GET /company_notifications/new
  def new
    @company_notification = CompanyNotification.new
  end

  # GET /company_notifications/1/edit
  def edit; end

  # POST /company_notifications
  # POST /company_notifications.json
  def create
    @company_notification = CompanyNotification.new(company_notification_params)
    respond_to do |format|
      if @company_notification.save
        format.html { redirect_to companies_path, notice: 'Company notification was successfully created.' }
        format.json { render :show, status: :created, location: @company_notification }
      else
        format.html { render :new }
        format.json { render json: @company_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company_notifications/1
  # PATCH/PUT /company_notifications/1.json
  def update
    respond_to do |format|
      if @company_notification.update(company_notification_params)
        format.html { redirect_to companies_path, notice: 'Company notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_notification }
      else
        format.html { render :edit }
        format.json { render json: @company_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company_notifications/1
  # DELETE /company_notifications/1.json
  def destroy
    @company_notification.destroy
    respond_to do |format|
      format.html { redirect_to companies_path, notice: 'Company notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_company_notification
    @company_notification = CompanyNotification.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def company_notification_params
    {
        user: Parse::User.find(params[:user_id]),
        company: Company.find(params[:company_id]),
    }
  end
end
