class EmployeesController < ApplicationController
  before_action :set_employee, only: %i[show edit update destroy]
  before_action :set_user
  layout 'admin'
  require 'will_paginate/array'

  # GET /employees
  # GET /employees.json
  def index
    i_am_admin? ? @employees = Parse::Query.new('Employee') : @employees = @user.company.employees.all
    @employees.where :company.id => params[:company_id] if params[:company_id].present?
    @employees.where :company.id => @user.company.id if !@user.is_admin && @employees.class.to_s == 'Parse::Query'
    @employees.where :first_name.like => params[:name].to_s if params[:name].present?
    @employees.sort_by(&:first_name)
    @employees = @employees.result if @employees.class.to_s == 'Parse::Query'
    @employees = @employees.paginate(page: params[:page], per_page: 20)
    @companies = Company.all
    @companies = Parse::Query.new('Company').where id: @user.company.id
  end

  # GET /employees/1
  # GET /employees/1.json
  def show; end

  # GET /employees/new
  def new
    @companies = Company.all if @user.is_admin
    @companies = Parse::Query.new('Company').where id: @user.company.id if @user.is_supervisor
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
    @companies = Company.all if @user.is_admin
    @companies = Parse::Query.new('Company').where id: @user.company.id if @user.is_supervisor
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)
    @employee.company = Company.find(params[:employee][:company])
    @employee.can_view_all_areas = false if @employee.can_view_all_areas.nil?
    respond_to do |format|
      if @employee.save
        format.html { redirect_to employee_path(@employee), notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      @employee.first_name = params[:employee][:first_name]
      @employee.last_name = params[:employee][:last_name]
      @employee.email =  params[:employee][:email]
      @employee.phone =  params[:employee][:phone]
      @employee.rut = params[:employee][:rut]
      @employee.company = Company.find(params[:employee][:company])
      @employee.can_view_all_areas = params[:employee][:can_view_all_areas]
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_employee
    @employee = Employee.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def employee_params
    # Parse server dont allow strong parameters
    data = {
      first_name: params[:employee][:first_name],
      last_name: params[:employee][:last_name],
      email: params[:employee][:email],
      phone: params[:employee][:phone],
      rut: params[:employee][:rut],
      company: params[:employee][:company],
      can_view_all_areas: params[:employee][:can_view_all_areas]
    }
  end
end
