class NonconformitiesController < ApplicationController
  before_action :set_nonconformity, only: %i[show edit update destroy finish_action]
  before_action :set_user
  layout 'admin'
  require 'will_paginate/array'
  # GET /nonconformities
  # GET /nonconformities.json
  def index
    if i_am_admin? || @user.can_view_all
      @nonconformities = Parse::Query.new('Nonconformity')
      @nonconformities.where :employee.id => params[:employee_id] if params[:employee_id].present?
      @nonconformities.where :status.id => params[:status_id] if params[:status_id].present?
      @nonconformities.where :area.id => params[:area_id] if params[:area_id].present?
    else
      @nonconformities = []
      @user.company.employees.each do |employee|
        employees = Nonconformity.where employee: employee
        @nonconformities += employees.all order: :priority.asc
      end
    end
    #search by company
    if params[:company_id].present?
      @nonconformities_arr = []
      @nonconformities.each do |non|
        if non.try(:employee).try(:company).try(:id) == params[:company_id]
          @nonconformities_arr <<non
        end
      end
      @nonconformities = @nonconformities_arr
    end
    @nonconformities = @nonconformities.sort_by(&:created_at).reverse!.sort_by(&:priority)
    @nonconformities = @nonconformities.paginate(page: params[:page], per_page: 10)
  end

  # GET /nonconformities/1
  # GET /nonconformities/1.json
  def show
    if @nonconformity.status.name == 'Pendiente'
      @nonconformity.viewed = "#{@user.first_name} #{@user.last_name}"
      @nonconformity.created_at_viewed = Time.now
      @nonconformity.status = Status.first_or_create(name: 'En revisión')
      @nonconformity.priority = 1
      pp @nonconformity
      @nonconformity.save
      @viewed = @nonconformity.viewed
      @created_at_viewed = @nonconformity.created_at_viewed
    else
      @viewed = @nonconformity.viewed
      @created_at_viewed = @nonconformity.created_at_viewed
      if @nonconformity.status.name != 'Finalizado'
        @nonconformity.viewed = "#{@user.first_name} #{@user.last_name}"
        @nonconformity.created_at_viewed = Time.now
        @nonconformity.status = Status.first_or_create(name: 'En revisión')
        pp @nonconformity
        @nonconformity.save
      end
    end
  end

  def finish_action
    @nonconformity.status = Status.first_or_create(name: 'Finalizado')
    @nonconformity.priority = 2
    respond_to do |format|
      if @nonconformity.save!
        format.html { redirect_to @nonconformity, notice: 'Nonconformity was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # GET /nonconformities/new
  def new
    @nonconformity = Nonconformity.new
  end

  # GET /nonconformities/1/edit
  def edit; end

  # POST /nonconformities
  # POST /nonconformities.json
  def create
    @nonconformity = Nonconformity.new(nonconformity_params)
    employee = Employee.find(params[:nonconformity][:employee])
    area = Area.find(params[:nonconformity][:area])
    status = Status.find(params[:nonconformity][:status])
    @nonconformity.employee = employee
    @nonconformity.area = area
    @nonconformity.status = status
    @nonconformity.priority = status_to_id(status.name)
    respond_to do |format|
      if @nonconformity.save
        format.html { redirect_to @nonconformity, notice: 'Nonconformity was successfully created.' }
        format.json { render :show, status: :created, location: @nonconformity }
      else
        format.html { render :new }
        format.json { render json: @nonconformity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nonconformities/1
  # PATCH/PUT /nonconformities/1.json
  def update
    respond_to do |format|
      @nonconformity.description = params[:nonconformity][:description]
      @nonconformity.lat = params[:nonconformity][:lat]
      @nonconformity.lng = params[:nonconformity][:lng]
      employee = Employee.find(params[:nonconformity][:employee])
      area = Area.find(params[:nonconformity][:area])
      status = Status.find(params[:nonconformity][:status])
      @nonconformity.employee = employee
      @nonconformity.area = area
      @nonconformity.status = status
      @nonconformity.priority = status_to_id(status.name)

      if @nonconformity.save
        format.html { redirect_to @nonconformity, notice: 'Nonconformity was successfully updated.' }
        format.json { render :show, status: :ok, location: @nonconformity }
      else
        format.html { render :edit }
        format.json { render json: @nonconformity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nonconformities/1
  # DELETE /nonconformities/1.json
  def destroy
    @nonconformity.destroy
    respond_to do |format|
      format.html { redirect_to nonconformities_url, notice: 'Nonconformity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_nonconformity
    @nonconformity = Nonconformity.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def nonconformity_params
    data = {
      description: params[:nonconformity][:description],
      lat: params[:nonconformity][:lat],
      lng: params[:nonconformity][:lng]
    }
  end

  def status_to_id(status)
    case status
    when 'Pendiente'
      0
    when 'En revisión'
      1
    else
      2
      end
end
end
