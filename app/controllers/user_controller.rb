class UserController < ApplicationController
  before_action :set_user, except: %i[sign_in create_session]

  def sign_up
    @company = Parse::Query.new('Company').where :id => @user.company.id
  end

  def sign_in; end

  def delete_user
    Parse::User.find(params[:id]).destroy
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'Has borrado un usuario con éxito!' }
    end
  end

  def create_user
    user = Parse::User.new(user_params)
    user.company = Company.find(params[:user][:company])
    begin
      user.signup!
      respond_to do |format|
        format.html { redirect_to users_path, notice: 'Has añadido un usuario con éxito!' }
      end
    rescue => error
      respond_to do |format|
        format.html { redirect_to users_path, notice: error }
      end
    end
  end

  def end_session
    user = Parse::User.session(session[:session_token])
    user.logout
    respond_to do |format|
      format.html { redirect_to sign_in_path }
    end
  rescue => error
    respond_to do |format|
      format.html { redirect_to sign_in_path }
    end
  end

  def create_session
    user = Parse::User.login(params[:user][:email], params[:user][:password])
    session[:session_token] = user.session_token
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Has iniciado sesion con exito!' }
    end
  rescue => error
    respond_to do |format|
      format.html { redirect_to sign_in_path, notice: 'Combinacion email/contraseña incorrectos' }
    end
  end

  private

  def user_params
    # Parse server dont allow strong parameters
    is_supervisor = false
    can_view_all = false
    params[:user][:is_admin] = params[:user][:is_admin] == '1' ? true : false
    if params[:user][:company].present? && !params[:user][:company].equal?('')
      if params[:user][:is_admin]
        is_supervisor = true
        params[:user][:is_admin] = false
      end
    else
      can_view_all = true unless params[:user][:is_admin]
    end
    p is_supervisor
    data = {
      username: params[:user][:email],
      firstName: params[:user][:first_name],
      lastName: params[:user][:last_name],
      email: params[:user][:email],
      password: params[:user][:password],
      is_admin: params[:user][:is_admin],
      is_supervisor: is_supervisor,
      company: Company.find(params[:user][:company]),
      can_view_all: can_view_all
    }
  end

  def login_params
    # Parse server dont allow strong parameters
    data = {
      email: params[:user][:email],
      password: params[:user][:password]
    }
  end
end
